<?php
// $Id: newarlig.admin.inc 43 2011-09-27 12:44:41Z ahmtsari@gmail.com $

/**
 * @File
 *  Newarlig admin hooks / callbacks & service functions
 */

/**
 * Form builder. Configure Newarlig.
 *
 * @ingroup forms
 * @see system_settings_form
 */
function newarlig_admin_settings() {
    $form['newarlig_saison'] = array(
        '#type' => 'textfield',
        '#title' => t('Current Saison'),
        '#description' => t('Enter an numerc id of the actual saison.'),
        '#default_value' => variable_get('newarlig_saison', 0),
        '#size' => 10,
    );    
    $form['newarlig_result'] = array(
        '#type' => 'textfield',
        '#title' => t('Point of right result'),
        '#description' => t('How many point will be added if a prediction right in result?'),
        '#default_value' => variable_get('newarlig_result', 5),
        '#size' => 1,
    );
    $form['newarlig_score'] = array(
        '#type' => 'textfield',
        '#title' => t('Point of right score'),
        '#description' => t('How many point will be added if a prediction right in score?'),
        '#default_value' => variable_get('newarlig_score', 5),
        '#size' => 1,
    );
    $form['newarlig_diff'] = array(
        '#type' => 'textfield',
        '#title' => t('Point of right score difference'),
        '#description' => t('How many point will be added if a prediction right in score difference?'),
        '#default_value' => variable_get('newarlig_diff', 2),
        '#size' => 1,
    );
    $form['recalculate'] = array(
        '#type' => 'fieldset',
        '#title' => t('Recalculate all games'),
        '#description' => t('If you change the points of game, you should recalculate users scores for all games. <em>The button recalculates user predictions of all finished games.</em>'),
    );
    $form['recalculate']['start'] = array(
        '#type' => 'submit',
        '#value' => t('Recalculate all games'),
        '#submit' => array('_save_points'),
    );
    return system_settings_form($form);
}

