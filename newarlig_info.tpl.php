<?php
// $Id: newarlig_info.tpl.php 23 2010-12-28 16:58:03Z ahmtsari@gmail.com $
/**
 * Template to display newarlig nodes.
 *
 * Fields available:
 * $home_team: cleaned plain text string
 * $away_team: cleaned plain text string
 */
?>
<div class="newarlig_info">
    <?php if (time() < $node->game_time) { ?>
        <h2><?php print t('Coming match:');} ?></h2>
    <?php print format_date($node->game_time) ?> -
    <?php print t('@hometeam vs @awayteam',
        array('@hometeam' => $node->home_team, '@awayteam' => $node->away_team)); ?>
    <?php if (time() > $node->game_time && $node->is_finished) {
       print $node->home_score.' : '.$node->away_score;
    }?>
    <!--
    <?php if (!$page): ?>
        [<a href="<?php print url('node/'. $node->nid); ?>">
        <?php if (time() > $node->game_time) {
            print t('View');
        } else {print t('Predict');}?></a>]
    <?php endif; ?>
    -->
</div>
