<?php
// $Id: newarlig.inc 43 2011-09-27 12:44:41Z ahmtsari@gmail.com $
/**
 * @file
 * Modules spesific funtions other than hooks. I is generated to reduce
 * main module file. So, it will be not included until they will be called.
 */

function newarlig_overview($op = 'list') {
    global $user;

    $sql = "SELECT g.game_time, g.home_team, g.away_team, g.home_score, g.away_score, "
        ."g.nid, p.home_score p_score1, p.away_score p_score2, p.prediction_score "
        ."FROM {game} g JOIN {game_prediction} p ON g.nid = p.nid "
        ."WHERE p.uid = %d AND g.saison_id = %d "
        ."UNION "
        ."SELECT g.game_time, g.home_team, g.away_team, g.home_score, g.away_score, "
        ."g.nid, NULL, NULL, 0 "
        ."FROM {game} g WHERE saison_id = %d AND nid NOT in (SELECT nid from {game_prediction} "
        ."WHERE uid = %d) ";
    switch ($op) {
        case 'edit':
            $sql = "SELECT g.game_time, g.home_team, g.away_team, g.home_score, g.away_score, "
                ."g.nid, p.home_score p_score1, p.away_score p_score2, p.prediction_score "
                ."FROM {game} g JOIN {game_prediction} p ON g.nid = p.nid "
                ."WHERE p.uid = %d AND g.saison_id = %d AND g.game_time > " . time()
                ." UNION "
                ."SELECT g.game_time, g.home_team, g.away_team, g.home_score, g.away_score, "
                ."g.nid, NULL, NULL, 0 "
                ."FROM {game} g WHERE saison_id = %d AND nid NOT in (SELECT nid from {game_prediction} "
                ."WHERE uid = %d) "
                ." AND g.game_time > " . time()
                ." ORDER BY game_time";   
            $count_qry = "SELECT COUNT(*) FROM {game} WHERE saison_id = " 
                . variable_get('newarlig_saison', 0)
                ." AND game_time > " . time();            
            break;
        case 'list':
            $sql = "SELECT g.game_time, g.home_team, g.away_team, g.home_score, g.away_score, "
                ."g.nid, p.home_score p_score1, p.away_score p_score2, p.prediction_score "
                ."FROM {game} g JOIN {game_prediction} p ON g.nid = p.nid "
                ."WHERE p.uid = %d AND g.saison_id = %d AND g.game_time <= " . time()
                ." UNION "
                ."SELECT g.game_time, g.home_team, g.away_team, g.home_score, g.away_score, "
                ."g.nid, NULL, NULL, 0 "
                ."FROM {game} g WHERE saison_id = %d AND nid NOT in (SELECT nid from {game_prediction} "
                ."WHERE uid = %d) "
                ." AND g.game_time <= " . time()
                ." ORDER BY game_time DESC";       
            $count_qry = "SELECT COUNT(*) FROM {game} WHERE saison_id = " 
                . variable_get('newarlig_saison', 0)
                ." AND game_time <= " . time();            
            break;
    }
//    watchdog('newarlig_overview', $sql, WATCHDOG_DEBUG);
    $result = pager_query($sql, 9, 0, $count_qry, $user->uid, variable_get('newarlig_saison', 0), 
            variable_get('newarlig_saison', 0), $user->uid);
    $data = array();
    $rank = 1;
    while ($row = db_fetch_array($result)) {
        $game_time = $row['game_time'];
        $row['game_time'] = format_date($row['game_time']);
        if ($game_time > time()) {
            $node_obj = (object) $row;
            //Prediction form
            unset($row['home_score']);
            unset($row['away_score']);
            unset($row['p_score1']);
            unset($row['p_score2']);
            unset($row['prediction_score']);
            $row['form'] = array(
                'data' => drupal_get_form('newarlig_predict_table_form_' . $rank, $node_obj),
                'colspan' => 5,
            );
        }
        unset($row['nid']);
        $data[] = array_merge(array("rank" => $rank++), $row);
    }
    $header = array(
        '#',
        t('Date'),
        t('Home Team'),
        t('Away Team'),
        array('data' => t('Skor'), 'colspan' => 2),
        array('data' => t('Pred.'), 'colspan' => 2),
        t('P.'),
    );
    $content = theme('table', $header, $data);
    if (user_access('create game nodes')) {
        $content = drupal_get_form('newarlig_table_form') . $content;
    }    
    return $content . theme('pager');
}

function newarlig_table_form ($form_state, $node = NULL) {
    $token = drupal_get_token() . '_form_state';
    if (isset($_SESSION[$token])) $form_state = $_SESSION[$token];
    $date_format = variable_get('newarlig_date_format', NEWARLIG_DATE_FORMAT);
    $form['#theme'] = 'newarlig_table_form';
    $form['#submit'][] = '_newarlig_table_form_submit';
    $form['game_time'] = array(
        '#type' => 'textfield',
        '#title' => t('Start Date/Time'),
        '#description' => t('When will the game start. Format: %time.',
            array('%time' => format_date(time(), 'custom', $date_format))),
        '#maxlength' => 25,
        '#default_value' => isset($form_state['values']['game_time']) ?
            $form_state['values']['game_time'] :
            format_date(time(), 'custom', $date_format),
    );
    if (module_exists('date_popup')) {
      // Make this a popup calendar widget if Date Popup module is enabled.
      $form['game_time']['#type'] = 'date_popup';
      $form['game_time']['#date_format'] = $date_format;
      $form['game_time']['#date_year_range'] = '0:+10';
      unset($form['game_time']['#description']);
      unset($form['game_time']['#maxlength']);
    }    
    $form['home_team'] = array(
        '#type' => 'textfield',
        '#title' => t('Home team'),
        '#description' => t('Enter home team of the game.'),
        '#size' => 30,
        '#maxlength' => 80,
    );
    $form['away_team'] = array(
        '#type' => 'textfield',
        '#title' => t('Away team'),
        '#description' => t('Enter away team of the game.'),
        '#size' => 30,
        '#maxlength' => 80,
    );    
    $form['saison_id'] = array('#type' => 'hidden', '#value' => variable_get('newarlig_saison', 0),);    
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Add'),
    );    
    return $form;
}

function theme_newarlig_table_form($form) {
    $output = '<table><tr><td>';
    $output .= drupal_render($form['game_time']) . '</td><td>';
    $output .= drupal_render($form['home_team']). '</td><td>';
    $output .= drupal_render($form['away_team']). '</td><td>';
    $output .= '<br/>' . drupal_render($form['submit']). '</td><td>';
    $output .= drupal_render($form). '</td></tr></table>';
    Return $output;
}

function _newarlig_table_form_submit ($form, $form_state) {
    global $user;
    // Create a new game node
    //$form_state = array();
    module_load_include('inc', 'node', 'node.pages');
    $node = array('type' => 'game');
    $form_state['values']['op'] = t('Save');
    $form_state['values']['name'] = $user->name;
    $token = drupal_get_token() . '_form_state';
    $_SESSION[$token] = $form_state;
    drupal_execute('game_node_form', $form_state, (object)$node);  
}
function newarlig_predict_table_form ($form_state, $node = NULL) {
    $form['#theme'] = 'newarlig_predict_table_form';
    $form['#submit'][] = '_newarlig_prediction_form_submit';
    $form['#validate'][] = '_newarlig_prediction_form_validate';
    $form['homescore'] = array(
        '#type' => 'textfield',
        '#size' => 2,
        '#default_value' => isset($node->p_score1) ? $node->p_score1 : '',
    );
   $form['awayscore'] = array(
        '#type' => 'textfield',
        '#size' => 2,
        '#default_value' => isset($node->p_score2) ? $node->p_score2 : '',
    );
    $form['nid'] = array(
        '#type' => 'hidden',
        '#value' => $node->nid,
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => isset($node->p_score1) ? t('Update') : t('Predict'),
    );
    return $form;
}
function theme_newarlig_predict_table_form($form) {
    $output = '<table><tr><td>';
    $output .= drupal_render($form['homescore']) . '</td><td>';
    $output .= drupal_render($form['awayscore']). '</td><td>';
    $output .= drupal_render($form['submit']). '</td><td>';
    $output .= drupal_render($form). '</td></tr></table>';
    Return $output;
}

function theme_newarlig_node_predictions($items) {
    $output = '<hr/><h3>' . t('Predictors') . '</h3>';
    if (!empty($items)) {
        $output .= theme('table', array(t('User'), t('Home Score'), t('Away Score'), t('Prediction Score')), $items);
        $output .= theme('pager');
    }
    return $output;
}
